﻿using GildedRose.Domain.Enums;

namespace GildedRose.Domain.Entities
{
    public sealed class Item
    {
        public int Id { get; set; }
        
        public string Nome { get; set; }

        public int? PrazoValidade { get; set; }

        public int Qualidade { get; set; }
        
        public ClasseItem? ClasseItem { get; set; }
    }
}
