using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using GildedRose.Domain.Entities;
using GildedRose.Domain.Enums;
using GildedRose.Domain.Interfaces;

namespace GildedRose.Domain.Services
{
    public class ItemService : IItemService
    {
        private readonly IItemRepository _repository;

        public ItemService(IItemRepository repository)
        {
            this._repository = repository;
        }
        
        public void AtualizarQualidade(int dias = 1)
        {
            if (dias <= 0)
                return;
            
            var itens = _repository.Todos();
            Expression<Func<Item, Item>> aux = i => AtualizarItem(i);
            for (int i = 0; i < dias; i++)
            {
                itens = itens.Select(aux);
            }
            this._repository.Atualizar(itens);
        }

        public IQueryable<Item> Todos()
        {
            return this._repository.Todos();
        }

        public Item ObterPorId(int id)
        {
            return this._repository.ObterPorId(id);
        }

        public bool Adicionar(Item i)
        {
            return this._repository.Adicionar(i);
        }

        public bool Atualizar(Item i)
        {
            return this._repository.Atualizar(i);
        }

        public bool Remover(int id)
        {
            return this._repository.Remover(id);
        }

        private static Item AtualizarItem(Item item)
        {
            var novoItem = item switch
            {
                {ClasseItem: ClasseItem.NORMAL} => AtualizarNormal(item),
                {ClasseItem: ClasseItem.LENDARIO} => AtualizarLendario(item),
                {ClasseItem: ClasseItem.REVERSO} => AtualizarReverso(item),
                {ClasseItem: ClasseItem.INGRESSO} => AtualizarIngresso(item),
                {ClasseItem: ClasseItem.CONJURADO} => AtualizarConjurado(item),
                _ => throw new Exception("Classe de Item Inválida")
            };
            return novoItem;
        }
        
        private static Item AtualizarNormal(Item item)
        {
            item.PrazoValidade--;
            if (item.PrazoValidade >= 0)
            {
                item.Qualidade -= 1;
            }
            else if (item.PrazoValidade < 0)
            {
                item.Qualidade -= 2;
            }
            
            item.Qualidade = item.Qualidade <= 0 ? 0 : item.Qualidade;
            return item;
        }
        
        private static Item AtualizarLendario(Item item)
        {
            return item;
        }
        
        private static Item AtualizarReverso(Item item)
        {
            item.PrazoValidade--;
            if (item.PrazoValidade < 0)
            {
                item.Qualidade += 2;
            }
            else
            {
                item.Qualidade += 1;
            }

            item.Qualidade = item.Qualidade > 50 ? 50 : item.Qualidade;
            return item;
        }
        
        private static Item AtualizarIngresso(Item item)
        {
            item.PrazoValidade--;
            if (item.PrazoValidade < 0)
            {
                item.Qualidade = 0;
            }
            else if (item.PrazoValidade < 5)
            {
                item.Qualidade += 3;
            }
            else if (item.PrazoValidade < 10)
            {
                item.Qualidade += 2;
            }
            else
            {
                item.Qualidade += 1;
            }

            item.Qualidade = item.Qualidade >= 50 ? 50 : item.Qualidade;
            return item;
        }
        
        private static Item AtualizarConjurado(Item item)
        {
            item.PrazoValidade--;
            item.Qualidade -= 2;
            item.Qualidade = item.Qualidade < 0 ? 0 : item.Qualidade;
            return item;
        }
    }
}