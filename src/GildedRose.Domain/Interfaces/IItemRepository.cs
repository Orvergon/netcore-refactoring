using System.Collections;
using System.Collections.Generic;
using System.Linq;
using GildedRose.Domain.Entities;

namespace GildedRose.Domain.Interfaces
{
    public interface IItemRepository
    {
        public IQueryable<Item> Todos();
        public Item ObterPorId(int id);
        public bool Adicionar(Item i);
        public bool Atualizar(Item i);
        public bool Atualizar(IQueryable<Item> itens);
        public bool Remover(int id);
    }
}