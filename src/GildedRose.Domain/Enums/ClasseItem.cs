namespace GildedRose.Domain.Enums
{
    public enum ClasseItem
    {
        NORMAL,
        REVERSO,
        LENDARIO,
        CONJURADO,
        INGRESSO
    }
}