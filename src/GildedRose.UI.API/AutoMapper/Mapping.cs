using AutoMapper;
using GildedRose.Domain.Entities;
using GildedRose.UI.API.ViewModels.Item;

namespace GildedRose.UI.API.AutoMapper
{
    public class Mapping : Profile
    {
        public Mapping()
        {
            CreateMap<AdicionaItemViewModel, Item>();
            CreateMap<AtualizaItemViewModel, Item>();
        }
    }
}