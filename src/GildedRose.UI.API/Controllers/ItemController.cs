using System;
using System.Collections.Generic;
using AutoMapper;
using GildedRose.Domain.Entities;
using GildedRose.Domain.Interfaces;
using GildedRose.UI.API.ViewModels.Item;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.CodeAnalysis;

namespace GildedRose.UI.API.Controllers
{
    [ApiController]
    [Route("item")]
    public class ItemController : Controller
    {
        private readonly IItemService _itemService;
        private readonly IMapper _mapper;
        public ItemController(IItemService itemService, IMapper mapper)
        {
            this._itemService = itemService;
            this._mapper = mapper;
        }
        
        [HttpGet]
        [Route("Todos")]
        public IEnumerable<Item> Todos()
        {
            return _itemService.Todos();
        }

        [HttpGet]
        [Route("ObterPorId")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(Item))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public ActionResult<Item> ObterPorId(int id)
        {
            var item = this._itemService.ObterPorId(id);
            if (item == null)
            {
                return NotFound();
            }
            return Ok(item);
        }

        [HttpGet]
        [Route("Adicionar")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public ActionResult Adicionar([FromQuery]AdicionaItemViewModel i)
        {
            if (ModelState.IsValid)
            {
                var item = this._mapper.Map<Item>(i);
                if (this._itemService.Adicionar(item))
                {
                    return Ok();
                }
            }

            return BadRequest();
        }
        
        [HttpGet]
        [Route("Atualizar")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public ActionResult Atualizar([FromQuery]AtualizaItemViewModel i)
        {
            if (ModelState.IsValid)
            {
                var item = this._mapper.Map<Item>(i);
                if (this._itemService.Atualizar(item))
                {
                    return Ok();
                }
            }

            return BadRequest();
        }
        
        [HttpGet]
        [Route("Remover")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public ActionResult Remover(int id)
        {
            if (this._itemService.ObterPorId(id) == null)
            {
                return BadRequest();
            }

            if (this._itemService.Remover(id))
            {
                return Ok();
            }
            return BadRequest();
        }
        
        [HttpGet]
        [Route("AtualizarValores")]
        public void AtualizarValores(int dias = 1)
        {
            this._itemService.AtualizarQualidade(dias);
        }
    }
}