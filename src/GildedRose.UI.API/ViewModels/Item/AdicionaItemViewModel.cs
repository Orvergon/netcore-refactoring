using System.ComponentModel.DataAnnotations;
using GildedRose.Domain.Enums;

namespace GildedRose.UI.API.ViewModels.Item
{
    public class AdicionaItemViewModel
    {
        [Required]
        public string? Nome { get; set; }

        [Required]
        public int? PrazoValidade { get; set; }

        [Required]
        [Range(0, 50)]
        public int? Qualidade { get; set; }
        
        [Required]
        public ClasseItem? ClasseItem { get; set; }
    }
}