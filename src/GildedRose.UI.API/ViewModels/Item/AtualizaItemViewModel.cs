using System.ComponentModel.DataAnnotations;
using GildedRose.Domain.Enums;

namespace GildedRose.UI.API.ViewModels.Item
{
    public class AtualizaItemViewModel
    {
        [Required]
        public int? Id { get; set; }
        
        public string? Nome { get; set; }

        public int? PrazoValidade { get; set; }

        [Range(0, 50)]
        public int? Qualidade { get; set; }
        
        public ClasseItem? ClasseItem { get; set; }
    }
}