using GildedRose.Data.Repositories;
using GildedRose.Domain.Interfaces;
using GildedRose.Domain.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace GildedRose.IoC
{
    public class Bootstrapper
    {
        public static void RegisterServices(IServiceCollection services, IConfiguration configuration)
        {
            /*Services*/
            services.AddScoped<IItemRepository, ItemRepository>();
            
            /*Repositories*/
            services.AddScoped<IItemService, ItemService>();
        }
    }
}