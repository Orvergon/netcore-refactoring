using System;
using System.Collections.Generic;
using System.Linq;
using GildedRose.Data.Context;
using GildedRose.Domain.Entities;
using GildedRose.Domain.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace GildedRose.Data.Repositories
{
    public class ItemRepository : IItemRepository
    {
        private GildedRoseDbContext _db;

        public ItemRepository(GildedRoseDbContext db)
        {
            this._db = db;
        }

        public IQueryable<Item> Todos()
        {
            return this._db.Items;
        }

        public Item ObterPorId(int id)
        {
            return this._db.Items.FirstOrDefault(x => x.Id == id);
        }

        public bool Adicionar(Item i)
        {
            try
            {
                this._db.Add<Item>(i);
                var a = this._db.SaveChanges();
                if (a != 0)
                {
                    return true;
                }
            }
            catch (Exception _)
            {
                return false;
            }

            return false;
        }

        public bool Atualizar(Item i)
        {
            try
            {
                var entry = this._db.Entry(i);
                this._db.Items.Attach(i);
                entry.State = EntityState.Modified;
                var a = this._db.SaveChanges();
                if (a != 0)
                {
                    return true;
                }
            }
            catch (Exception _)
            {
                return false;
            }
            return false;
        }
        
        public bool Atualizar(IQueryable<Item> itens)
        {
            try
            {
                this._db.UpdateRange(itens);
                var a = this._db.SaveChanges();
                if (a != 0)
                {
                    return true;
                }
            }
            catch (Exception _)
            {
                return false;
            }
            return false;
        }

        public bool Remover(int id)
        {
            try
            {
                this._db.Items.Remove(this.ObterPorId(id));
                var a = this._db.SaveChanges();
                if (a != 0)
                {
                    return true;
                }
            }
            catch (Exception _)
            {
                return false;
            }
            return false;
        }
    }
}