using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace GildedRose.Data.Context
{
    public class GildedRoseDbContextDesignFactory : IDesignTimeDbContextFactory<GildedRoseDbContext>
    {
        public GildedRoseDbContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<GildedRoseDbContext>();
            optionsBuilder.UseSqlite("Data Source=/home/orvergon/src/netcore-refactoring-dojo/src/GildedRose.Data/Context/db.sqlite3");

            return new GildedRoseDbContext(optionsBuilder.Options);
        }
    }
}