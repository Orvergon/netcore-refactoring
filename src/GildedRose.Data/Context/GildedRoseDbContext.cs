﻿using System.Reflection;
using System.Text;
using GildedRose.Domain.Entities;
using GildedRose.Domain.Enums;
using Microsoft.EntityFrameworkCore;

namespace GildedRose.Data.Context
{
    public class GildedRoseDbContext : DbContext
    {
        public DbSet<Item> Items { get; set; }

        public GildedRoseDbContext(DbContextOptions contextOptions) : base(contextOptions)
        {
            
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            var targetAssembly = Assembly.GetExecutingAssembly();
            modelBuilder.ApplyConfigurationsFromAssembly(targetAssembly);
            modelBuilder.Entity<Item>().HasData(
                    new Item {Id = -1, ClasseItem = ClasseItem.NORMAL, Nome = "Corselete +5 DEX", PrazoValidade = 10, Qualidade = 20},
                    new Item {Id = -2, ClasseItem = ClasseItem.REVERSO, Nome = "Queijo Brie Envelhecido", PrazoValidade = 2, Qualidade = 0},
                    new Item {Id = -3, ClasseItem = ClasseItem.NORMAL, Nome = "Elixir do Mangusto", PrazoValidade = 5, Qualidade = 7},
                    new Item {Id = -4, ClasseItem = ClasseItem.LENDARIO, Nome = "Sulfuras, a Mão de Ragnaros", PrazoValidade = 0, Qualidade = 80},
                    new Item {Id = -5, ClasseItem = ClasseItem.LENDARIO, Nome = "Sulfuras, a Mão de Ragnaros", PrazoValidade = -1, Qualidade = 80},
                    new Item {Id = -6, ClasseItem = ClasseItem.INGRESSO, Nome = "Ingressos para o concerto do TAFKAL80ETC", PrazoValidade = 15, Qualidade = 20 },
                    new Item {Id = -7, ClasseItem = ClasseItem.INGRESSO, Nome = "Ingressos para o concerto do TAFKAL80ETC", PrazoValidade = 10, Qualidade = 49 },
                    new Item {Id = -8, ClasseItem = ClasseItem.INGRESSO, Nome = "Ingressos para o concerto do TAFKAL80ETC", PrazoValidade = 5, Qualidade = 49 },
                    new Item {Id = -9, ClasseItem = ClasseItem.CONJURADO, Nome = "Bolo de Mana Conjurado", PrazoValidade = 3, Qualidade = 6}
                );
        }
    }
}