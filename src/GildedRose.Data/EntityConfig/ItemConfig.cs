using GildedRose.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GildedRose.Data.EntityConfig
{
    public class ItemConfig : IEntityTypeConfiguration<Item>
    {
        public void Configure(EntityTypeBuilder<Item> builder)
        {
            builder.ToTable("Item");
            
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Nome).IsRequired().HasMaxLength(200);
            builder.Property(x => x.Qualidade).IsRequired();
            builder.Property(x => x.ClasseItem).IsRequired();
            builder.Property(x => x.PrazoValidade).IsRequired(false);
        }
    }
}